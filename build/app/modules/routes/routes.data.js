"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.routes = void 0;
const reviews_routes_1 = require("../reviews/reviews.routes");
const routes_types_1 = require("./routes.types");
exports.routes = [
    new routes_types_1.Route("/review", reviews_routes_1.reviewRouter),
    // new Route("/asset", AssetRouter)
];
//# sourceMappingURL=routes.data.js.map