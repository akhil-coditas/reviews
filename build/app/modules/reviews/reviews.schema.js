"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ReviewSchema {
    constructor() {
        this.Reviews = [];
    }
    create(Review) {
        this.Reviews.push(Object.assign({}, Review));
        return true;
    }
    get() {
        return this.Reviews;
    }
}
const ReviewsSchema = new ReviewSchema();
exports.default = ReviewsSchema;
//# sourceMappingURL=reviews.schema.js.map