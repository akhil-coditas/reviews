export interface IReview {
    reviewerName: string,
    restrauntName: "ABC" | "DEF" | "GHI" | "JKL" | "MNO",
    Food: number,
    Ambiance: number,
    Service: number,
    Cleanliness: number,
    Overall: number
}
