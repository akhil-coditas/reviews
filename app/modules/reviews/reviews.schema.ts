import { IReview } from "./reviews.types";

class ReviewSchema {
    Reviews: IReview[] = [];

    create(Review: IReview) {
        this.Reviews.push({...Review});
        return true;
    }

    get() {
        return this.Reviews;
    }
}

const ReviewsSchema = new ReviewSchema();

export default ReviewsSchema;
