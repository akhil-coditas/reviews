import { Application, json, NextFunction, Request, Response } from "express";
import { Routes } from "./routes.types";
import { routes } from "./routes.data";
import { Response as ResponseHandler } from "../../utility/response-handler";


export const registerRoutes = (app: Application) => {
    app.use(json());

    for(let route of routes) {
        app.use(route.path, route.router);
    }

    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        res.status(err.statusCode || 500).send(new ResponseHandler(null, err))
    })

}
