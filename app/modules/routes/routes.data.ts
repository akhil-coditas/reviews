import { adminRouter } from '../admin/admin.routes';
import { managerRouter } from '../manager/manager.routes';
import { reviewRouter } from '../reviews/reviews.routes';
import { Route, Routes } from './routes.types';

export const routes: Routes = [
    new Route("/review", reviewRouter),
    new Route("/admin", adminRouter),
    new Route("/manager", managerRouter)
];