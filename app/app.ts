import express from "express";
import { registerRoutes } from "./modules/routes/routes.registry";

export const startServer = () => {
    const app = express();
    const { PORT } = process.env;

    registerRoutes(app);

    app.listen( PORT, ()=> console.log(`Server Started at PORT ${PORT}`)
    )
} 